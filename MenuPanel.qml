﻿import QtQuick 2.12
import QtQuick.Controls 2.12

Item
{
	anchors.fill: parent

	Column
	{
		Button
		{
			text: qsTr("ListView")
			enabled: true
			onClicked: stack.push("qrc:/ListViewTest.qml")
		}

		Button
		{
			text: qsTr("PathView")
			enabled: false
			onClicked: stack.push(Qt.resolvedUrl("PathViewTest.qml"))
		}

		Button
		{
			text: qsTr("CanvasView")
			enabled: false
			onClicked: stack.push(Qt.resolvedUrl("CanvasTest.qml"))
		}

		Button
		{
			text: qsTr("BalloonView")
			enabled: true
			onClicked: stack.push("qrc:/BalloonView.qml")
		}
	}
}
