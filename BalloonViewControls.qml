﻿import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle {
	anchors.bottom: parent.bottom
	anchors.left: parent.left
	anchors.margins: 1
	anchors.right: parent.right
	border.width: 2
	color: "white"
	height: parent.height * 0.15

	Text {
		anchors.centerIn: parent
		text: "Add another balloon"
	}

	MouseArea {
		anchors.fill: parent
		hoverEnabled: true
		onClicked: {
			balloonView.model.append({"balloonWidth": Math.floor(Math.random() * 200 + 100)})
			balloonView.positionViewAtIndex(balloonView.count -1, ListView.End)
		}
		onEntered: {
			parent.color = "#8ac953"
		}
		onExited: {
			parent.color = "white"
		}
	}
}
