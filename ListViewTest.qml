﻿import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle
{
	height: 800
	width: 200
	color: "green"

	ListView
	{
		height: parent.height
		width: parent.width
		model: ContactModel {}
		delegate: contactDelegate
		highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
		focus: true
	}

	Component
	{
		id: contactDelegate
		Row
		{
			width: ListView.view.width
			height: contactImage.height
			Image
			{
				id: contactImage
				width: 50
				height: 50
				source: icon ? icon : "qrc:/pics/raccoon_rapier_cropped.png"
			}
			Column
			{
				Text { text: '<b>Name:</b> ' + name }
				Text { text: '<b>Number:</b> ' + number }
			}
		}
	}
}
