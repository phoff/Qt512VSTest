﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import TestCpp 1.0

Item {
	id: balloonViewWrapper
	height: parent.height
	width: parent.width

	ListView
	{
		anchors.bottom: controls.top
		anchors.top: parent.top
		width: parent.width
		id: balloonView
		model: ContactModel {}
		spacing: 5
		delegate: TextBalloon
		{
			anchors.right: index % 2 == 0 ? undefined : parent.right
			height: 60
			rightAligned: index % 2 == 0 ? false : true
			width: balloonWidth

			RowLayout
			{
				Layout.alignment: Qt.AlignVCenter
				Layout.margins: 5

				Image
				{
					id: contactImage
					source: icon ? icon : "qrc:/pics/raccoon_rapier_cropped.png"
					Layout.maximumHeight: parent.height
					Layout.maximumWidth: parent.height
					Layout.alignment: Qt.AlignVCenter
				}

				ColumnLayout
				{
					RowLayout
					{
						Text
						{
							text: '<b>Name:</b> '
						}
						Text
						{
							text: name ? name : "rocky"
						}
					}

					RowLayout
					{
						Text
						{
							text: '<b>Number:</b> '
						}
						Text
						{
							text: number ? number : "9"
						}
					}
				}
			}
		}
	}

	BalloonViewControls
	{
		id: controls
	}
}

