﻿import QtQuick 2.12

ListModel
{
	ListElement
	{
		name: "Bill Smith"
		number: "555 3264"
		icon: "qrc:/pics/Barkin'_Bill_Smith.png"
		balloonWidth: 200
	}
	ListElement
	{
		name: "John Brown"
		number: "555 8426"
		icon: "qrc:/pics/John Brown.png"
		balloonWidth: 120
	}
	ListElement
	{
		name: "Sam Wise"
		number: "555 0473"
		icon: "qrc:/pics/samwise.png"
		balloonWidth: 175
	}
	ListElement
	{
		name: "test"
		number: "6"
		balloonWidth: 150
	}
}
