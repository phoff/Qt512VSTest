import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Window 
{
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button
	{
		id: backButton
		enabled: stack.depth > 1
		text: qsTr("back")
		onClicked: stack.pop()
		width: 100
		height: 20
	}

	StackView
	{
		id: stack
		initialItem: MenuPanel { id: mainMenu }
		anchors
		{
			top: backButton.bottom
			bottom: Window.bottom
			left: Window.left
			right: Window.right
		}
	}
}
